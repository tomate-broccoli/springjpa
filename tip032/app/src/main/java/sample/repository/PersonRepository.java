package sample.repository;

// import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.data.jpa.repository.Modifying;
// import org.springframework.data.jpa.repository.Query;
// import org.springframework.data.repository.query.Param;
// import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import javax.persistence.PersistenceContext ;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;

import sample.entity.Person;

// @Transactional
@Repository
public interface PersonRepository extends JpaRepository<Person, String>{
    // nothing.
}
// public class PersonRepository extends SimpleJpaRepository<Person, String>{
// public abstract class PersonRepository implements JpaRepository<Person, String>{
// public class PersonRepository<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> {

//    @PersistenceContext 
//     EntityManager em;
/* 
    SimpleJpaRepository simpleJpaRepo;

    @Autowired
    public void getSimpleJpaRepository(){
        this.simpleJpaRepo = new SimpleJpaRepository(Person.class, this.em);
    }
*/
/*
    // @Autowired
    PersonRepository(Class <T> domainClass, EntityManager em){
        super(domainClass, em);
        this.em = em;
    }

    @Override
    public <S extends T> S save(S e){
    // public Person save(Person e){
        // return e.isNew() ? insert(e) : (Person)simpleJpaRepo.save(e);
        return ((Person)e).isNew() ? (S)insert((Person)e) : super.save(e);
    }

    Person insert(Person e){
        Query q = em.createNativeQuery("insert into PERSON value(?) returning ROWID", String.class);
        q.setParameter(1, e.name);
        q.executeUpdate();
        String rowid = (String)q.getSingleResult();
        return e;
    }
}
*/
