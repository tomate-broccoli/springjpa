package sample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sample.entity.Person;
import sample.repository.PersonRepository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AppService {

    @Autowired
    PersonRepository personRepo;

    @PersistenceContext
    EntityManager em;

    public List<Person> findAll(){
        return personRepo.findAll();
    }

    @Transactional
    public void regist(String name) {
        System.out.println("** name:"+name);
        // Person p = new Person().toBuilder().name(name).build();
        // personRepo.save(p);
        // String rowid = (String)em.createNativeQuery("insert into Person(name) values (?) returning rowid")
        // em.createNativeQuery("insert into Person(name) values (?)")
        //    .setParameter(1, name)
        //    // .getSingleResult()
        //    .executeUpdate()
        //;
        String v = "";
        Person p = (Person)em.createNativeQuery("insert into Person(name) values (?)", Person.class)
            .setParameter(1, name)
            .getSingleResult()
        ;
        System.out.println("*** p:" + p);
    }
}
