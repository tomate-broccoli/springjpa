package org.example;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonId implements Serializable{
    @Column(name="NAME")
    public String name;

    @Column(name="LOCATION")
    public String location;
}