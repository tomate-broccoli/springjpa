package org.example;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="PERSON", schema="SCOTT")
public class Person implements Serializable {
    @EmbeddedId PersonId id;

    @Column(name="VAL")
    public Float val;

    @Column(name="DATE_TIME")
    public Date dateTime;
}
