package org.example;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MyService{
    @Autowired
    PersonRepository repo;

	@Transactional
    public void doStart(){
		PersonId pid = new PersonId(){{
  			name = "hoge";
			location = "japan";
		}};

		Optional<Person> opt = repo.findById(pid);
		if(!opt.isPresent()) return;

        Person p = opt.get();
		p.val = 246.8f;
		p.dateTime = new Date();
		repo.save(p);
    }
}