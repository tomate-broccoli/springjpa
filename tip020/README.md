# README.md

## Gradle
```
mkdir tip020
cd $_
gradle init
gradle test

```

## Edit
- build.gradle
- Java Code

## run
```
gradle run
```

## Oracle設定
```
docker login registry.gitlab.com
docker pull registry.gitlab.com/tomate-broccoli/gitpod_tip011
docker run -d --env-file ../oracle.env --name ORCL2 -p 1521:1521 registry.gitlab.com/tomate-broccoli/gitpod_tip011
```

```
docker exec -it ORCL2 sqlplus system/oracle21C3@MYORCL

create user SCOTT identified by tiger
account unlock ;

grant connect, resource to scott ;

alter user scott quota UNLIMITED on users ;
exit;
```

```
docker exec -it ORCL2 sqlplus scott/tiger@MYORCL

create table PERSON (
     name varchar2(20),
     location varchar2(20),
     val number(10,5),
     date_time date
) ;

insert into PERSON values('hoge', 'japan', 123.45, sysdate) ;
commit ;
select * from PERSON;
exit;

```