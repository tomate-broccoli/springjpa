package sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sample.entity.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, String>{
    // nothing.
}
