package tip028.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tip028.entity.Person;
import tip028.repository.PersonRepository;

@Service
public class AppService {

    @Autowired
    PersonRepository personRepo;

    public Person select(){
        List<Person> list = findAll();
        return list.get(0);
    }

    public List<Person> findAll(){
        return personRepo.findAll();
    }

    public void regist(String name) {
        Person p = new Person().toBuilder().name(name).build();
        personRepo.save2(p);
    }
}
