package tip028.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import tip028.entity.Person;
import tip028.service.AppService;

@RestController
public class AppController {
    
    @Autowired 
    AppService appService;

    @RequestMapping("/")
    public String hoge(){
        Person p = appService.select();
        return p.getName();
    }

    @GetMapping("/regist")
    public List<Person> regist(@RequestParam("name") String name){
        appService.regist(name);
        return appService.findAll();
    }
}
