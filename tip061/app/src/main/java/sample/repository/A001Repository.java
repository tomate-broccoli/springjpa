package sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sample.entity.A001;

@Repository
public interface A001Repository extends JpaRepository<A001, Integer>{
    //
}
