package sample;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;

@SpringBootApplication
public class AppMain {

  public static void main(String[] args) {
    SpringApplication.run(AppMain.class, args);
  }

}