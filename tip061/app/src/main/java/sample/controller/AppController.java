package sample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import sample.entity.A001;
import sample.entity.B001;
import sample.service.AppService;

@RestController
public class AppController {
    
    @Autowired 
    AppService appService;

    @RequestMapping("/a001")
    public List<A001> findAllA001(){
        return appService.findAllA001();
    }

    @RequestMapping("/b001")
    public List<B001> findAllB001(){
        return appService.findAllB001();
    }

}
