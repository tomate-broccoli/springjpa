package tip019.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.SQLInsert;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Table(name="PERSON", schema="SCOTT")
// @SQLInsert( sql = "insert into SCOTT.PERSON (name) values (?)")
public class Person implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    // @Column(name="ROWID", nullable = true, updatable = false, insertable = false)
    @Column(name="ROWID")
    private String id;

    // @Id
    @Column(name="NAME")
    public String name;
}
