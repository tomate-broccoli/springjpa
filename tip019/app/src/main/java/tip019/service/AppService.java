package tip019.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tip019.entity.Person;
import tip019.repository.PersonRepository;

@Service
public class AppService {

    @Autowired
    PersonRepository personRepo;

    public List<Person> findAll(){
        return personRepo.findAll();
    }

    public void regist(String name) {
        System.out.println("** name:"+name);
        Person p = new Person().toBuilder().name(name).build();
        personRepo.save(p);
    }
}
