package tip019.dialect;

import org.hibernate.cfg.Environment;
import org.hibernate.dialect.Oracle12cDialect;
import org.hibernate.dialect.identity.IdentityColumnSupport;

public class MyOracle12cDialect extends Oracle12cDialect {

    @Override
    public IdentityColumnSupport getIdentityColumnSupport() {
        // System.out.println("** MyOracle12Dialect.getIdentityColumnSupport() called!!!");
        return new MyOracle12cIdentityColumnSupport();
        // return super.getIdentityColumnSupport();
    }

    @Override
	public String getNativeIdentifierGeneratorStrategy() {
	    // String str = super.getNativeIdentifierGeneratorStrategy();
        // System.out.println("** MyOracle12Dialect.getNativeIdentifierGeneratorStrategy(): return:"+ str);
		// return "identity";
        // return str;
	    return super.getNativeIdentifierGeneratorStrategy();
	}

    @Override
	protected void registerDefaultProperties() {
		super.registerDefaultProperties();
        // System.out.println("** MyOracle12Dialect.registerDefaultProperties() called!!!");
		//ORG: getDefaultProperties().setProperty( Environment.USE_GET_GENERATED_KEYS, "true" );
		// getDefaultProperties().setProperty( Environment.USE_GET_GENERATED_KEYS, "false" );
	}

}