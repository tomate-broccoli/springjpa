package tip019.dialect;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.hibernate.dialect.Dialect;
// import org.hibernate.dialect.identity.GetGeneratedKeysDelegate;
import org.hibernate.dialect.identity.Oracle12cGetGeneratedKeysDelegate;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.PostInsertIdentityPersister;

//NG: public class MyOracle12cGetGeneratedKeysDelegate extends GetGeneratedKeysDelegate{
public class MyOracle12cGetGeneratedKeysDelegate extends Oracle12cGetGeneratedKeysDelegate{

    public MyOracle12cGetGeneratedKeysDelegate(PostInsertIdentityPersister persister, Dialect dialect) {
        super(persister, dialect);
        // System.out.println("** MyOracle12cGetGeneratedKeysDelegate:"+persister.getRootTableKeyColumnNames()[0]);
    }

    @Override
	protected PreparedStatement prepare(String insertSQL, SharedSessionContractImplementor session) throws SQLException {
        // System.out.println("** MyOracle12cGetGeneratedKeysDelegate.prepare: insertSQL:"+ insertSQL);  // rowid設定済み
        return super.prepare(insertSQL, session);
        // String sql = insertSQL.replace("rowid,", "").replace("default,", "");
        // return super.prepare(sql, session);
    }
    
}
