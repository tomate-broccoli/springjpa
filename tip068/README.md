# README


## sqlite

```
sqlite3
.database
.open sample.db.sqlite3
.database
create table foo ( id number, chr01 varchar(20), num01 number(10), dat01 date);
insert into foo values(1, 'ふう', 12345, current_date);
select * from foo
-- commit;

create table bar ( id number, chr01 varchar(20), num01 number(10), dat01 date, foo_id number);
insert into bar values(10, 'ばあ', 123450, current_date, 1);
select * from bar
-- commit;

.q

```