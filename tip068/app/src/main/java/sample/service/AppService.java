package sample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;
import sample.entity.Foo;
import sample.repository.FooRepository;

@Log4j2
@Service
public class AppService {

    @Autowired
    FooRepository FooRepo;

    public List<Foo> getFoo(){
        List<Foo> list = FooRepo.findAll();
        log.debug(list);
        return list;
    }
}
