package sample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sample.entity.Foo;
import sample.service.AppService;

@Controller
public class AppController {

    @Autowired
    AppService service;

    @ResponseBody
    @RequestMapping("/foo")
    public List<Foo> foo(){
        return service.getFoo();
    }
}
