package sample.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Foo {

    @Id
    @Column(name = "ID")
    Integer id;

    @Column(name = "CHR01")
    String chr01;

    @Column(name = "NUM01")
    Float num01;

    // @Column(name = "DAT01")
    // Date dat01;
}
