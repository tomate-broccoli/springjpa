package sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sample.entity.Foo;

public interface FooRepository extends JpaRepository<Foo, Integer> {

}
