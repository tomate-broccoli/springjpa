package sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sample.entity.B001;

@Repository
public interface B001Repository extends JpaRepository<B001, Integer>{
    //
}
