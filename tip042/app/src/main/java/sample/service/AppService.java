package sample.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sample.entity.A001;
import sample.entity.B001;
import sample.repository.A001Repository;
import sample.repository.B001Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class AppService {

    @Autowired
    A001Repository a001Repo;

    @Autowired
    B001Repository b001Repo;

    public List<A001> findAllA001(){
        return a001Repo.findAll();
    }

    public List<B001> findAllB001(){
        return b001Repo.findAll();
    }

}
