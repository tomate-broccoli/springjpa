#  めも

## Oracle 
- docker login registry.gitlab.com
- docker pull registry.gitlab.com/tomate-broccoli/gitpod_tip011
- docker run -d --env-file ../oracle.env --name ORCL2 -p 1521:1521 registry.gitlab.com/tomate-broccoli/gitpod_tip011

## ユーザ作成

- docker exec -it ORCL2 sqlplus system/oracle21C3@MYORCL

```sql
create user SCOTT identified by tiger
account unlock ;

grant connect, resource to scott ;

alter user scott quota UNLIMITED on users ;
```

## テーブル作成

- docker exec -it ORCL2 sqlplus scott/tiger@MYORCL

```sql
create table A001(
     id number,
     chr01 varchar2(50)
) ;
insert into A001 values(1, 'abcde');
insert into A001 values(2, 'xyz');
commit ;

create table B001(
     id number,
     chr02 varchar2(50),
     a001_id number
) ;

insert into B001 values(10, 'あいうえお', 1);
insert into B001 values(20, 'かきくけこ', 2);
commit ;
```