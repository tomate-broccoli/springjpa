package sample.repository;

// import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.data.jpa.repository.Modifying;
// import org.springframework.data.jpa.repository.Query;
// import org.springframework.data.repository.query.Param;
// import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import sample.entity.Person;
import java.util.List;

// @Transactional
@Repository
public interface PersonRepository extends JpaRepository<Person, String>{
    // @Modifying
    // @Query(nativeQuery = true, value="insert into PERSON(name) values(:#{#e.name})")
    // int save2(@Param("e") Person entity);

    Person findByRowid(String rowid);

    List<Person> findAllByName(String name);
}
