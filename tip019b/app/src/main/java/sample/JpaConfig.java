package sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.persistence.EntityManagerFactory;
import org.springframework.context.annotation.Bean;
import javax.annotation.PostConstruct;

@Configuration
@EnableTransactionManagement
public class JpaConfig{

    private final EntityManagerFactory entityManagerFactory;
    
    public JpaConfig(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }
    
    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
    
    @PostConstruct
    public void disableCache() {
        entityManagerFactory.getCache().evictAll();
    }

}