package sample.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.data.domain.Persistable;
import org.hibernate.annotations.RowId; 
// import org.hibernate.annotations.SQLInsert;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.hibernate.annotations.NaturalId;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
// @RowId("ROWID")
@Table(name="PERSON", schema="SCOTT")
public class Person implements Serializable /*, Persistable<String> */ {
    
    // @Id
    @Column(name="NAME")
    public String name;

    @Id
    // @GeneratedValue
    // @NaturalId(mutable=true)
    @Column(name="ROWID", nullable = true, updatable = false, insertable = false)
    public String rowid;

//    @Override
//    public String getId() {
//        return this.rowid;
//    }
//    @Override
//    public boolean isNew() {
//        return this.rowid == null ? true : false;
//    }
}
