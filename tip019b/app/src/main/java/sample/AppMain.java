package sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.persistence.EntityManagerFactory;
import org.springframework.context.annotation.Bean;
import javax.annotation.PostConstruct;
import org.springframework.context.annotation.Import;

@Import(JpaConfig.class)
@SpringBootApplication
public class AppMain {

  public static void main(String[] args) {
    SpringApplication.run(AppMain.class, args);
    // SpringApplication.run(JpaConfig.class, args);
  }

}