package sample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sample.entity.Person;
import sample.repository.PersonRepository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.CacheMode;
// import javax.transaction.Transactional;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Session ;
import org.hibernate.Transaction;
import java.util.stream.Collectors;

@Service
public class AppService {

    @Autowired
    PersonRepository personRepo;

    @PersistenceContext
    private EntityManager entityManager;

    public List<Person> findAll(){
        return personRepo.findAll();
    }
    public List<Person> findAll2(){
        entityManager.clear();
        entityManager.getEntityManagerFactory().getCache().evict(Person.class);
        //NG: entityManager.flush();
        //NG: entityManager.setCacheMode(CacheMode.IGNORE);
        //NG: String sql = "SELECT * FROM PERSON a";
        //NG: String sql = "SELECT name FROM PERSON a";
        String sql = "SELECT name, rowidtochar(rowid) as \"ROWID\" FROM PERSON where rowid is not null";
        return (List<Person>) entityManager.createNativeQuery(sql, Person.class)
            // .setHint("javax.persistence.cache.storeMode", "REFRESH")
            // .setHint("org.hibernate.cacheable", false)
            // .setHint("org.hibernate.cacheMode", "IGNORE")    // NORMAL,IGNORE,GET,PUT,REFRESH
            .getResultList();
    }

    public void regist1(String name) {
        System.out.println("** name:"+name);
        Person p = new Person().toBuilder().name(name).build();
        personRepo.save(p);
    }
    public Person regist2(String name){
        List<String> rowidList = getRowidList(name);
        Person p = new Person().toBuilder().name(name).build();
        personRepo.save(p);
        String rowid= getRowidList2(name, rowidList);
        return personRepo.findByRowid(rowid);
    }
    public Person regist3(String name){
        List<String> rowidList = getRowidList(name);
        Person p = new Person().toBuilder().name(name).build();
        // personRepo.save(p);
        save(p);
        String rowid= getRowidList2(name, rowidList);
        System.out.println("** rowid:"+ rowid);
        Person person = findByRowid(rowid);
        System.out.println("** person:"+ person);
        return person;

    }
    public Person regist(String name){
        List<Person> rowidList = personRepo.findAllByName(name);
        Person p = new Person().toBuilder().name(name).build();
        save(p);
        String rowid= getRowidList2(name, rowidList.stream().map(e->e.rowid).collect(Collectors.toList()));
        System.out.println("** rowid:"+ rowid);
        Person person = personRepo.findByRowid(rowid);
        System.out.println("** person:"+ person);
        return person;
    }
    public Person findByRowid(String rowid){
        entityManager.clear();
        // entityManager.getEntityManagerFactory().getCache().evict(Person.class);
        // entityManager.flush();
        // String sql = "SELECT name, rowidtochar(rowid) as \"ROWID\" FROM PERSON WHERE \"ROWID\" = :rowid";
        String sql = "SELECT name, rowidtochar(rowid) as \"ROWID\" FROM PERSON WHERE ROWID = :rowid";
        return (Person) entityManager.createNativeQuery(sql, Person.class)
            .setParameter("rowid", rowid)
            .getSingleResult();
    }

    public List<String> getRowidList1(String name){
        String sql = "SELECT ROWID FROM PERSON WHERE NAME = :name";
        return (List<String>) entityManager.createNativeQuery(sql)
            .setParameter("name", name)
            .getResultList();
    }

    public List<String> getRowidList(String name){
        String sql = "SELECT rowidtochar(rowid) as \"ROWID\" FROM PERSON WHERE NAME = ?1";
        return (List<String>) entityManager.createNativeQuery(sql)
            .setParameter(1, name)
            .getResultList();
    }

    public String getRowidList2(String name, List<String> rowidList){
        System.out.println("** name:"+ name);
        System.out.println("** rowidList:"+ rowidList);
        String sql = "SELECT rowidtochar(rowid) as \"ROWID\" FROM PERSON WHERE NAME = :name and ROWID not in (:rowidList)";
        return (String) entityManager.createNativeQuery(sql)
            .setParameter("name", name)
            .setParameter("rowidList", rowidList)
            .getSingleResult();
    }

    @Transactional
    void save(Person p){
        Session session = entityManager.unwrap(Session.class);
        Transaction txn = session.beginTransaction();
        String sql = "insert into PERSON(name) values(:name)";
        entityManager.createNativeQuery(sql)
            .setParameter("name", p.name)
            .executeUpdate()
        ;
        txn.commit();
    }

}
