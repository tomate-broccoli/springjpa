package sample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sample.entity.Person;
import sample.repository.PersonRepository;

@Service
public class AppService {

    @Autowired
    PersonRepository personRepo;

    public List<Person> findAll(){
        return personRepo.findAll();
    }

    public void regist(String name) {
        System.out.println("** name:"+name);
        Person p = new Person().toBuilder().name(name).build();
        personRepo.save(p);
    }
}
