package org.example;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MyService{
    @Autowired
    PersonRepository repo;

	@Transactional
    public void doStart(){

/* 複数行の場合エラーになる
		//NG: Caused by: org.hibernate.HibernateException: Duplicate row was found and `ASSERT` was specified
		// Optional<Person> opt = repo.findById(pid);
 */
/* KEYが重複している行の更新はエラーになる
		List<Person> list= repo.findByIdName(pid.getName());
		System.out.printf("*** list.size=%d.\n", list.size());    // *** list.size=2.
		list.forEach(System.out::println);                               // 2x Person(id=PersonId(name=hoge, location=japan), val=123.45, dateTime=2024-09-07 12:34:56.0)
        Person p = list.get(0);
		p.val = 135.79f;
		//NG: Caused by: org.hibernate.HibernateException: Duplicate identifier in table (scott.person) - org.example.Person#PersonId(name=hoge, location=japan)
		repo.save(p);
 */
/* エンティティのサブクラスでの登録はエラーになる
		pid.setLocation("tokyo");
        Person p = new Person(){{
			id = pid;
			val = 246.8f;
			dateTime = new Date();
		}};
		//NG: Caused by: org.hibernate.UnknownEntityTypeException: Unable to locate entity descriptor: org.example.MyService$2
		repo.save(p);
 */

		// pid.setLocation("tokyo");
        Person p = new Person();
  		p.name = "hoge";
		p.location = "japan";
		p.val = 246.8f;
		p.dateTime = new Date();
		//NG: Caused by: org.hibernate.id.IdentifierGenerationException: Identifier of entity 'org.example.Person' must be manually assigned before calling 'persist()'
		//NG: Caused by: java.sql.SQLException: ORA-17006: Invalid column name: rowid
		repo.save(p);
    }
}