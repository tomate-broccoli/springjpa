package org.example;

import java.io.Serializable;
import java.lang.annotation.Inherited;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="PERSON", schema="SCOTT")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROWID", insertable = false, updatable = false)
    public String rowid;

    @Column(name="NAME")
    public String name;

    @Column(name="LOCATION")
    public String location;

    @Column(name="VAL")
    public Float val;

    @Column(name="DATE_TIME")
    public Date dateTime;
}
